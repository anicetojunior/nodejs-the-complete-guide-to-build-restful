const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground')
  .then(() => console.log('Connected to MongoDB...'))
  .catch(err => console.error('Could not connect to MongoDB...', err));

const authorSchema = new mongoose.Schema({
  name: String,
  bio: String,
  website: String
});

const Author = mongoose.model('Author', authorSchema);

const Course = mongoose.model('Course', new mongoose.Schema({
  name: String,
  authors: [authorSchema]
  //author: {
  //  type: authorSchema,
  //  require: true
 // }
}));

async function createCourse(name, authors) {
  const course = new Course({
    name, 
    authors
  }); 
  
  const result = await course.save();
  console.log(result);
}

async function listCourses() { 
  const courses = await Course.find();
  console.log(courses);
}

async function addAuthor(courseId, author) {
  const course = await Course.findById(courseId);
  course.authors.push(author);
  course.save();
}

async function updateAuthor(courseId) {
  // const course = await Course.findById(courseId);
  // course.author.name = 'Aniceto'
  // course.save();

  const course = await Course.update({ _id: courseId }, {
    //$set: {
    //  'author.name': 'Joao Ninguem'
    //}
    $unset: {
      'author': ''
    }
  });
}

async function removeAuthor(courseId, authorId) {
  const course = await Course.findById(courseId);
  const author = course.authors.id(authorId);
  author.remove();
  course.save();
}

// addAuthor('5c376cfb8f95fe2b78cd0edd', new Author({ name: 'Renata'}));
removeAuthor('5c376cfb8f95fe2b78cd0edd', '5c376dcf7fcc592548aedb05');

//createCourse('Node Course', new Author({ name: 'Mosh' }));
//updateAuthor('5c3745ea4602e526a0e3cf98');
//createCourse('Node Course', [
//  new Author({ name: 'Aniceto'}),
//  new Author({ name: 'Aniceto Jr'})
//]);
//new Author({ name: 'Mosh' }));
