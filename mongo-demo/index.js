
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/playground')
    .then(() => console.log('Connected to MongoDB'))
    .catch(err => console.error('Could not connect to MongoDB...', err));

const courseSchema = new mongoose.Schema({
    name: { 
        type: String, 
        required: true,
        minlength: 5,
        maxlength: 255,
        //match: /pattern/
    },
    category: {
        type: String,
        required: true,
        enum: ['web','mobile','network'],
        lowercase: true,
        //uppercase: true,
        trim: true
    },
    author: String,
    tags: {
        type: Array,
        validate: {
            isAsync: true,
            validator: function(v, callback) {
                setTimeout(() => {
                    // Do some async work
                    const result = v && v.length > 0;
                    callback(result);
                }, 4000);
            },  
                // return v && v.length > 0;            },
            message: 'A course should have at least one tag.'
        }
    },
    date: { type: Date, default: Date.now },
    isPublished: Boolean,
    price: {
        type: Number,
        required: function () {
            return this.isPublished;
        },
        min: 10,
        max: 30,
        get: v => Math.round(v),
        set: v => Math.round(v)
    }
});
const Course = mongoose.model('Course', courseSchema);

async function createCourse() {
    const course = new Course({
        name: 'Angular Course',
        category: 'Web',
        author: 'Aniceto',
        tags: 'backend',
        isPublished: true,
        price: 15.8
    });

    try {
        const result = await course.save();
        console.log(result);    
    } catch (ex) {
        for (field in ex.errors)
            console.log(ex.errors[field].message);
    }
    
}

async function getCourses() {
    // eq (equal)
    // ne (not equal)
    // gt (greater than)
    // gte (greater than or equal to)
    // lt (less than)
    // lte (less than or equal to)
    // in 
    // nin (not in)

    //or
    //and

    const courses = await Course
        //.find({ author: 'Aniceto', isPublished: true })
        //.find({ price: { $gte: 10, $lte: 20 } })
        //.find({ price: { $in: [10, 15, 20] }})
        // .find()
        // Starts with Mosh
        .find({ author: /^Aniceto/})

        // Ends with Aniceto
        .find({ author: /Aniceto$/i }) //$ = ends the string / i = incensitive
        
        // Contains Aniceto
        //.find({ author: /.*Aniceto.*/ }) // .=0/*=more caracters
        .find({ _id: '5c35cfa41436c82b5011fd4d'})
        .or([{author: 'Aniceto'}, { isPublished: true }])
        .limit(10)
        .sort({ name: 1 })
        .select({ name: 1, tags: 1, price: 1})
    console.log(courses[0].price);
}

async function updateCourse(id) {
    // Approach: Query first
    // findById()
    // Modify its propertis
    // save()
    const course = await Course.findById(id);
    if (!course) return;

    course.isPublished = true;
    course.author = 'Another Author';

    const result = await course.save();
    console.log(result);

    //course.set({
    //    isPublished: true,
    //    author: 'Another Author'
    //});


    // Approach: update first
    // Update directly
    // Optionally: get the updated document
}

async function updateCourseUpdateFirst(id) {
    // Approach: Query first
    // findById()
    // Modify its propertis
    // save()
    const course = await Course.findByIdAndUpdate(id, {
        $set: {
            author: 'Aniceto Junin 2',
            isPublished: true
        }
    }, {new: true });
    
    console.log(course);
 
    //course.set({
    //    isPublished: true,
    //    author: 'Another Author'
    //});


    // Approach: update first
    // Update directly
    // Optionally: get the updated document
}

async function removeCourse(id) {
    //const result = await Course.deleteOne({ _id: id});
    const result = await Course.deleteMany({ _id: id});
    console.log(result);
}


//createCourse();
getCourses();

