
sendCustomerEmail();

async function sendCustomerEmail() {
  try {
    const c = await getCustomer();
    if(c.isGold) {
      const m = await getTopMovies();
    }    
    const s = await sendEmail(c.email, m);
  } catch (error) {
    console.log('Error', error.message);
  }
}

function getCustomer() {
  console.log('Searching the customer...');
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({ 
        id: 1, 
        name: 'Mosh Hamedani', 
        isGold: true, 
        email: 'email@email.com.br' 
      });
    }, 4000);  
  })  
}

function getTopMovies() {
  console.log('Getting the movies...');
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(['movie1', 'movie2']);
    }, 4000);
  });
}

function sendEmail(email, movies) {
  console.log('Sending the email...');
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(console.log(`The customer e-mail is:${email} and movies selected:${movies}`));
    }, 4000);
  });  
}